package ru.edu.task1.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
public class ComponentC {

    private boolean isInit;

    @Autowired
    public void init() {
        isInit = true;
    }


    public boolean isValid() {
        return isInit;
    }
}
